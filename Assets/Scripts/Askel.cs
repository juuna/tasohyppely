﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Askel : MonoBehaviour
{
    public AudioSource stepSound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("w") && stepSound.isPlaying == false)
        {
            stepSound.Play();
        }
        else
        {
            stepSound.Stop();
        }
    }
}
