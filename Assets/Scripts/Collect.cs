﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collect : MonoBehaviour
{
    public int score = 1;

    private void Start()
    {
        ScoringSystem.coinsAmount += 1;
    }

    public AudioSource collectSound;
    void OnTriggerEnter(Collider other)
    {
        ColldectCoin(score);
    }

    public void ColldectCoin(int score)
    {
        ScoringSystem.coinsAmount -= 1;
        collectSound.Play();
        ScoringSystem.theScore += score;
        Destroy(gameObject);
    }

    void Update()
    {
        transform.Rotate(0.5f, 0f, 0f * Time.deltaTime);
    }
}
