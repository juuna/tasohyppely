﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        ScoringSystem.theScore = 0;
        TimerCountdown.secondsLeft = 59;
        SceneManager.LoadScene(1);
    }

    public void Restart()
    {
        ScoringSystem.theScore = 0;
        TimerCountdown.secondsLeft = 59;
        SceneManager.LoadScene(LastScene.lastScene);

    }

    public void NextLevel()
    {
        ScoringSystem.theScore = 0;
        TimerCountdown.secondsLeft = 59;
        SceneManager.LoadScene(3);
    }

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

}
