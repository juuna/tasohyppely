﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clock : MonoBehaviour
{
    public AudioSource clockSound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        collectTime();
        clockSound.Play();
        Destroy(gameObject);
    }

    public void collectTime()
    {
        TimerCountdown.secondsLeft += 30;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0.5f, 0f, 0f * Time.deltaTime);
    }
}
