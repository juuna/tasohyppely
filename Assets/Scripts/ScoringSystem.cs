﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoringSystem : MonoBehaviour
{
    public GameObject scoreText;
    public static int theScore;
    public static int coinsAmount;

    public int coinsAmountEditorWiew;

    void Update()
    {
        coinsAmountEditorWiew = coinsAmount;
            scoreText.GetComponent<Text>().text = "SCORE:" + theScore;

        if (coinsAmount == 0)
        {
            GameEnding();
        }

        if(Input.GetKeyDown(KeyCode.P))
        {
            GameEnding();
        }
    }

    private void GameEnding()
    {
        LastScene.lastScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(2);
        Cursor.lockState = CursorLockMode.Confined;
        ScoringSystem.theScore = 0;
    }
}
